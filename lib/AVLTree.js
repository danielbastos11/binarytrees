/** Constructor
 * 
 * @class AVLTree
 * @constructor
 * 
 * @param {Function} comparator Function used to sort the BST
 * @param {object} [config] Defines settings for the creation of the node
 * @param {Integer} [config.key] Key to the root of the new BST
 * @param {Integer} [config.value] Value to the root of the new BST
 * @param {Integer} [config.balanceFactor] Initial Balancing Factor of the new BST
 * @param {BinarySearchTree} [config.parent] Parent tree of the new BST
 */ 
AVLTree = function(comparator, config){
	//Make the object a BinarySearchTree
	BalancedTree.call(this, comparator, config);
	
	this.balanceFactor = config && config.balanceFactor ? config.balanceFactor : 0;
}

// Inherit prototype from BalancedTree (and hence from BinarySearchTree)
AVLTree.prototype = Object.create(BalancedTree.prototype);
AVLTree.prototype.constructor = AVLTree;

/**
 * React to insertions in one of its descendants
 * 
 * @param {AVLTree} subtree The subtree from which a node has been removed
 */ 
AVLTree.prototype.justAddedDescendant = function(subtree){
	// Update Balancing Factor
	if(this.left == subtree)
		this.balanceFactor++;
	else if(this.right == subtree)
		this.balanceFactor--;
	else
		throw new InternalError('The given subtree is not a child of this tree. Key: ' + this.key);
	
	
	switch(this.balanceFactor){
		case 0:
			return;
		case 1:
			if(this.parent)
				this.parent.justAddedDescendant(this);
			return;
		case -1:
			if(this.parent)
				this.parent.justAddedDescendant(this);
			return;
			
		// Keep in mind that the node under the "this" keyword
		// kinda change when a rotation is performed. For more
		// information, see one of the methods for rotation
		case 2:
		{	
			if(this.left.balanceFactor < 0){
				// Perform rotation
				this.doubleRightRotation();
				
				// Adjust Balance Factor
				switch(this.balanceFactor){
					case 0:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 0;
						this.right.balanceFactor = 0;
						break;
					}
					case -1:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 1;
						this.right.balanceFactor = 0;
						break;
					}
					case 1:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 0;
						this.right.balanceFactor = -1;
					}
				}
			}
			else{
				// Perform rotation
				this.simpleRightRotation();
				
				// Adjust Balance Factor
				this.right.balanceFactor = 0;
				this.balanceFactor = 0;
			}
			return;
		}
		case -2:
		{
			if(this.right.balanceFactor >= 0){
				// Perform rotation
				this.doubleLeftRotation();
				
				// Adjust Balance Factor
				switch(this.balanceFactor){
					case 0:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 0;
						this.right.balanceFactor = 0;
						break;
					}
					case -1:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 1;
						this.right.balanceFactor = 0;

						break;
					}
					case 1:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 0;
						this.right.balanceFactor = -1;
					}
				}
			}
			else{
				// Perform rotation
				this.simpleLeftRotation();
				
				// Adjust Balance Factor
				this.left.balanceFactor = 0;
				this.balanceFactor = 0;
			}
			return;
		}
	}
}

/**
 * Prepare to deletion in one of its descendants
 * 
 * @param {AVLTree} subtree The subtree in which a node has been deleted
 */ 
AVLTree.prototype.willRemoveDescendant = function(subtree){
	// Update Balancing Factor
	if(this.left == subtree)
		this.balanceFactor--;
	else if(this.right == subtree)
		this.balanceFactor++;
	else
		throw new InternalError('The given subtree is not a child of this tree. Key: ' + this.key);
	
	switch(this.balanceFactor){
		case 0:
			if(this.parent)
				this.parent.willRemoveDescendant(this);
			return;
		case 1:
			return;
		case -1:
			return;
		case 2:
		{
			// Guarantees that a double rotation will be performed
			// if necessary
			if(this.left.balanceFactor < 0){
				// Perform the rotation
				this.doubleRightRotation();
				
				// Adjust Balance Factor
				switch(this.balanceFactor){
					case 0:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 0;
						this.right.balanceFactor = 0;
						break;
					}
					case 1:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 0;
						this.right.balanceFactor = -1;
						break;
					}
					case 2:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 1;
						this.right.balanceFactor = 0;
						break;
					}
						
				}
			}
			else{
				// Perform the rotation
				this.simpleRightRotation();
				
				// Adjust Balance Factor
				switch(this.balanceFactor)
				{
					case 0:
					{
						this.balanceFactor = -1;
						this.right.balanceFactor = 1;
						break;
					}
					case 1:
					{
						this.balanceFactor = 0;
						this.right.balanceFactor = 0;
						break;
					}
				}
			}
			
			if(this.parent && this.balanceFactor == 0)
				this.parent.willRemoveDescendant(this);
			return;
		}
		case -2:
		{
			// Guarantees that a double rotation will be performed
			// if necessary
			if(this.right.balanceFactor >= 0){
				// Adjust Balance Factor
				this.doubleLeftRotation();
				
				// Adjust Balance Factor
				switch(this.balanceFactor){
					case 0:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 0;
						this.right.balanceFactor = 0;
						break;
					}
					case 1:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 0;
						this.right.balanceFactor = -1;
						break;
					}
					case 2:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 1;
						this.right.balanceFactor = 0;
						break;
					}
						
				}
				
			}
			else{
				// Perform the rotation
				this.simpleLeftRotation();
				
				// Adjust Balance Factor
				switch(this.balanceFactor)
				{
					case 0:
					{	
						this.balanceFactor = 1;
						this.left.balanceFactor = -1;
						break;
					}
					case -1:
					{
						this.balanceFactor = 0;
						this.left.balanceFactor = 0;
						break;
					}
				}
			}
			
			if(this.parent && this.balanceFactor == 0)
				this.parent.willRemoveDescendant(this);
			return;
		}
	}
}

/**
 * Factory Method created for compatibility with the BinarySearchTree class
 * 
 * @param {Function} comparator Function used to sort the AVLTree
 * @param {object} [config] Defines settings for the creation of the node
 * @param {Integer} [config.key] Key to the root of the new AVLTree
 * @param {Integer} [config.value] Value to the root of the new AVLTree
 * @param {Integer} [config.balanceFactor] Initial Balancing Factor of the new AVLTree
 * @param {BinarySearchTree} [config.parent] Parent tree of the new BST
 * @return {AVLTree} New AVL Tree
 */ 
AVLTree.prototype.createNewChild = function(comparator, config){
	return new AVLTree(comparator, config);
}

/**
 * Steal the identity of the given node. Will still its children, key, value and balanceFactor.
 * 
 * @param node The node to be hijacked
 */
AVLTree.prototype.hijack = function(node){
	BalancedTree.prototype.hijack.call(this, node);
	this.balanceFactor = node.balanceFactor;
}

/**
 * Generate a String representation of the tree
 * 
 * @return {String} String representation of the tree
 */
AVLTree.prototype.toString = function(){
	return this.key + '(' + this.balanceFactor + ')';
}