/** Constructor
 * 
 * @class BalancedTree
 * @constructor
 * 
 * @param {Function} comparator Function used to sort the BST
 * @param {object} [config] Defines settings for the creation of the node
 * @param {Integer} [config.key] Key to the root of the new BST
 * @param {Integer} [config.value] Value to the root of the new BST
 * @param {BinarySearchTree} [config.parent] Parent tree of the new BST
 */ 
BalancedTree = function(comparator, config){
	BinarySearchTree.call(this, comparator, config);
}

BalancedTree.prototype = Object.create(BinarySearchTree.prototype);
BalancedTree.prototype.constructor = BalancedTree;

/**
 * Performs a Simple Right Rotation on the tree
 * 
 * @method rightRotation
 */ 
BalancedTree.prototype.simpleRightRotation = function(){
	// Prepare a new Node with the former 'this'
	var newRightConfig = {
		key: this.key,
		value: this.value,
		parent: this.parent,
		balanceFactor: this.balanceFactor
	};
	var newRight = this.createNewChild(this.comparator, newRightConfig);
	
	newRight.right = this.right;
	newRight.left = this.left.right;
	
	var formerLeft = this.left; // Store for deletion
	this.hijack(this.left);
	
	// Define children of right
	if(newRight.left)
		newRight.left.parent = newRight;
	if(newRight.right)
		newRight.right.parent = newRight;
	
	// Define the relation between newRight and the new root
	this.right = newRight;
	newRight.parent = this;
	
	delete formerLeft;
}

/**
 * Performs a Simple Left Rotation on the tree
 * 
 * @method leftRotation
 */ 
BalancedTree.prototype.simpleLeftRotation = function(){
	// Prepare a new Node with the former 'this'
	var newLeftConfig = {
		key: this.key,
		value: this.value,
		parent: this.parent,
		balanceFactor: this.balanceFactor
	};
	var newLeft = this.createNewChild(this.comparator, newLeftConfig);
	
	newLeft.left = this.left;
	newLeft.right = this.right.left;
	
	var formerRight = this.right; // Store for deletion
	this.hijack(this.right);
	
	// Define children of newLeft
	if(newLeft.right)
		newLeft.right.parent = newLeft;
	if(newLeft.left)
		newLeft.left.parent = newLeft;
	
	// Define the relation between newLeft and the new root
	this.left = newLeft;
	newLeft.parent = this;
	
	
	delete formerRight;
}

/**
 * Performs a Double Right Rotation on the tree
 * 
 * @method doubleRightRotation
 */

BalancedTree.prototype.doubleRightRotation = function(){
	// Prepare a new Node with the former 'this'
	var newRightConfig = {
		key: this.key,
		value: this.value,
		parent: this.parent,
		balanceFactor: this.balanceFactor
	};
	var newRight = this.createNewChild(this.comparator, newRightConfig);
	var newRoot = this.left.right;
	var newLeft = this.left;
	
	// No need to do this on newLeft, since only newRight is a brand new object, 
	// with no children associated to it
	newRight.right = this.right;
	if(newRight.right)
		newRight.right.parent = newRight;
	
	// Readjust the children of this according to the FB of newRoot
	
	this.hijack(newRoot);
	
	newRight.left = newRoot.right;
	if(newRight.left)
		newRight.left.parent = newRight;
	
	newLeft.right = newRoot.left;
	if(newLeft.right)
		 newLeft.right.parent = newLeft;
	
	this.left = newLeft;
	newLeft.parent = this;
	
	this.right = newRight;
	newRight.parent = this;
}

/**
 * Performs a Double Left Rotation on the tree
 * 
 * @method doubleRightRotation
 */

BalancedTree.prototype.doubleLeftRotation = function(){
	// Prepare a new Node with the former 'this'
	var newLeftConfig = {
		key: this.key,
		value: this.value,
		parent: this.parent,
		balanceFactor: this.balanceFactor
	};
	
	var newLeft = this.createNewChild(this.comparator, newLeftConfig);
	var newRoot = this.right.left;
	var newRight = this.right;
	
	// No need to do this on newRight, since only newLeft is a brand new object, 
	// with no children associated to it
	newLeft.left = this.left;
	if(newLeft.left)
		newLeft.left.parent = newLeft;
	
	// Readjust the children of this according to the FB of newRoot
	
	this.hijack(newRoot);
	
	newRight.left = newRoot.right;
	if(newRight.left)
		newRight.left.parent = newRight;
	
	newLeft.right = newRoot.left;
	if(newLeft.right)
		 newLeft.right.parent = newLeft;
	
	this.right = newRight;
	newRight.parent = this;
	
	this.left = newLeft;
	newLeft.parent = this;
}