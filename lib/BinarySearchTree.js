/**
 * Constructor
 * 
 * @class BinarySearchTree
 * @constructor
 * 
 * @param {Function} comparator Function used to sort the BST
 * @param {object} [config] Defines settings for the creation of the node
 * @param {Integer} [config.key] Key to the root of the new BST
 * @param {Integer} [config.value] Value to the root of the new BST
 * @param {BinarySearchTree} [config.parent] Parent tree of the new BST
 */ 

// Beware that instantiating a BST without a key/value pair creates a Node that will 
// return "null" during a search-like algorithm.
BinarySearchTree = function (comparator, config)
{	
    // Assures that if a key is given, then the value is also present
    if(config && ((config.key == undefined && config.value != undefined) ||
                  (config.key != undefined && config.value == undefined)))
    {
        throw new Error('The presence of a key or a value on a BST node makes the another one mandatory');
    }
   
    this.key = config ? config.key : undefined;
    this.value = config ? config.value : undefined;
    this.parent = (config && config.parent instanceof BinarySearchTree) ? config.parent : undefined;
    
    if(!comparator instanceof Function)
        throw new TypeError("You gotta gimme a function, man!");
    
    this.comparator = comparator;
    
    // The "right" and "left" attributes might be added to this 
    // object during calls to the "add" method
}

/**
 * Add a new key/value pair to the BST
 * @method add
 * @param key Key being added to the tree
 * @param value Value being attached to the key
 */ 
BinarySearchTree.prototype.add = function(key, value)
{
    if(key == undefined && value == undefined)
        throw new Error("Can't add without key-value pair");
	else if((key == undefined && value != undefined) ||
            (key != undefined && value == undefined))
    {
        throw new Error('The presence of a key or a value on a BST node makes the another one mandatory');
	}
    
    // If this is the root to this tree
    if(this.key == undefined)
    {
        this.key = key;
        this.value = value;
		
		if(this.parent)
			this.parent.justAddedDescendant(this);
		
        return;
    }
    
    var comparison = this.comparator(key, this.key);
    
    if(comparison < 0)
    {
        if(this.left)
            this.left.add(key, value);
        else
		{
			var newLeft = this.createNewChild(this.comparator, 
											  {
												'key': key,
											  	'value': value,
											  	'parent': this
											  });
			
			this.left = newLeft;
			this.justAddedDescendant(newLeft);
		}
    }
    else if(comparison > 0)
    {
        if(this.right)
            this.right.add(key, value);
        else
		{
			var newRight = this.createNewChild(this.comparator, 
											  {
												'key': key,
											  	'value': value,
											  	'parent': this
											  });
			
			this.right = newRight;
			this.justAddedDescendant(newRight);
		}
    }
    else if(comparison == 0)
    {
        this.value = value;
    }
}

/**
 * Update a key to refer to a different value
 * 
 * @method update
 * @param key Key being added to the tree
 * @param value Value being attached to the key
 */ 
BinarySearchTree.prototype.update = BinarySearchTree.prototype.add;

/**
 * Remove a key/value pair from the tree
 * 
 * @method remove
 * @param key Key of the key/value pair you want to delete
 */ 
BinarySearchTree.prototype.remove = function(key)
{
    // Assures that this node is removed in case no "key" is provided
    var comparison = key ? this.comparator(key, this.key) : 0;
    
    if(comparison < 0)
    {
        if(this.left)
            this.left.remove(key);
		else
			return false;
    }
    else if(comparison > 0)
    {
        if(this.right)
            this.right.remove(key);
		else
			return false;
    }
    else
    {
        if(!(this.left || this.right))
        {
            if(this.parent)
            {
				this.parent.willRemoveDescendant(this);
				
                if(this.isLeftChild())
                    delete this.parent.left;
                else if(this.isRightChild())
                    delete this.parent.right;
            } else
            {
                this.key = undefined;
                this.value = undefined;
            }
        }
        else if(!this.right) // First "if" assures us that this node is not leaf.
                             // Hence, if right doesn't exist, left does
        {
            var hijacked = this.left;

            // "Steal" the identity of this.left, deleting itself
			
			this.hijack(hijacked);
			
			// Since no one will reference the former 'this.left', mark it for deletion
			delete hijacked;

			if(this.parent)
				this.parent.willRemoveDescendant(this);
        } else
        {
            if(!key || comparison == 0)
            {
               //Find a substitute for this node
               var subst = this.right;

               while(subst.left && subst.left.key)
                    subst = subst.left;

               this.key = subst.key;
               this.value = subst.value;

               subst.remove();
            }
        }
    }
}

/**
 * Find the value related to a given key
 * 
 * @method search
 * @parameter key The key used to store the pair/value tuple on the BTS
 * @return value The value wanted
 * @return undefined If key doesn't exist on the BTS
 */

BinarySearchTree.prototype.search = function(key)
{
    var comparison = this.comparator(key, this.key);
    
    if(comparison < 0)
        return this.left ? this.left.search(key) : undefined;
    if(comparison > 0)
        return this.right ? this.right.search(key) : undefined;
    if(comparison == 0)
        return this.value;
}


/**
 * Tells whether the Tree is empty
 * 
 * @method isEmpty
 * @return {Boolean} True if the tree is empty. False otherwise.
 * 
 */

BinarySearchTree.prototype.isEmpty = function()
{
	return (this.key == undefined);
}

/**
 * Find the value related the key of least value
 * 
 * @method minimum
 * @return minimum The value related to the key of least value stored on the tree
 * @return undefined In case the tree is empty
 */

BinarySearchTree.prototype.minimum = function()
{
    if(this.left)
        var min = this.left.minimum();
    
    // Make sure that a Node with no key set won't
    // return undefined. See the commentary below 
    // the Constructor
    return min != undefined ? min : this.key;
}

/**
 * Find the value related the key of greatest value
 * 
 * @method maximum
 * @return maximum The value related to the key of greatest value stored on the tree
 * @return undefined In case the tree is empty
 */

BinarySearchTree.prototype.maximum = function()
{
    if(this.right)
        var max = this.right.maximum();
    
    // Make sure that a Node with no key set won't
    // return undefined. See the commentary below 
    // the Constructor
    return max != undefined ? max : this.key;
}

/**
 * Find the value of the next sub-tree visited on an in-order traversal
 * 
 * @method successor
 * @return successor The next sub-tree to be visited
 * @return undefined In case the tree has the greatest key of the sub-trees available
 */ 

// The successor of a node can be:
//  a) Its right child, if it exist
//  b) The parent of the first of its ancestors that is a 
//     left-child, if the node doesn't have a right-child
//  c) Nonexistent, if the two other options refer to no node
BinarySearchTree.prototype.successor = function()
{
    if(this.right && this.right.value)
        return this.right.minimum();
    
    var temp = this;
    
    while(temp.parent)
    {
        if(temp.parent.left == temp)
            return temp.parent.key;
        
        temp = temp.parent;
    }
    
    return undefined;
}

/**
 * Find the value of the previous sub-tree visited on an in-order traversal
 * 
 * @method predecessor
 * @return predecessor The previous sub-tree to be visited
 * @return undefined In case the tree has the least key of the sub-trees available
 */ 

// The predecessor of a node can be:
//  a) Its left child, if it exist
//  b) The parent of the first of its ancestors that is a 
//     right-child, if the node doesn't have a left-child
//  c) Nonexistent, if the two other options refer to no node
BinarySearchTree.prototype.predecessor = function()
{
    if(this.left && this.left.value)
        return this.left.maximum();
    
    var temp = this;
    
    while(temp.parent)
    {
        if(temp.parent.right == temp)
            return temp.parent.key;
        
        temp = temp.parent;
    }
    
    return undefined;
}

//
//
//   METHODS FOR PRINTING THE TREE
//
//

/**
 * Print the current keys of the BST
 * 
 * @method print
 * @return {String} String of all the elements added to this BST
 */ 
BinarySearchTree.prototype.print = function()
{
    var output = '';
    
    if(this.left)
        output += this.left.print() + ' ';
    if(this.key != undefined)
        output += this.key;
    if(this.right)
        output += ' ' + this.right.print();
    
    return output;
}

/**
 * Print the current structure of the BST
 * 
 * @method print
 * @return {String} A string representation of the structure of the BST
 */
BinarySearchTree.prototype.printStructure = function()
{
    if(!this.key)
        return '';
    
    var output = '';
    
    var any = (this.left || this.right);
    
    if(this.key != undefined)
        output += this.toString();
    if(any)
        output += '[';
    if(this.left)
        output += this.left.printStructure();
    if(this.right)
        output += ', ' + this.right.printStructure();
    if(any)
        output += ']';
    
    return output;
}

//
//
//   UTILITY METHODS FOR SUBCLASSES
//
//

/**
 * Shows whether the tree is the left child of its parent tree
 * 
 * @method isLeftChild
 * @return {Boolean} True if the tree is a left-child. False otherwise.
 */
BinarySearchTree.prototype.isLeftChild = function(){
	if(!this.parent)
		return false;
	return (this == this.parent.left);
}

/**
 * Shows whether the tree is the right child of its parent tree
 * 
 * @method isRightChild
 * @return {Boolean} True if the tree is a right-child. False otherwise.
 */
BinarySearchTree.prototype.isRightChild = function(){
	if(!this.parent)
		return false;
	return (this == this.parent.right);
}

/**
 * Shows whether the node is the root of a tree
 * 
 * @method isRoot
 * @return {Boolean} True if the node is the root. False otherwise.
 */
BinarySearchTree.prototype.isRoot = function(){
	return !this.parent;
}

// Essa foi a coisa mais chata de fazer a árvore dessa forma "recursiva".
// Na hora de deletar algum nó (ou de fazer alguma rotação) eu tinha que
// manter a referência ao nó e não necessariamente poderia mudar o "parent.left"
// ou "parent.right", porque o nó poderia ser o raiz, por exemplo. A
// única solução que eu encontrei foi a de copiar todas as informações
// do nó substituto para o antigo nó e deletar o nó substituto, o que eu
// chamei de "hijack" ('sequestrar', em inglês)

/**
 * Steal the identity of the given node. Will still its children, key and value.
 * 
 * @param node The node to be hijacked
 */

BinarySearchTree.prototype.hijack = function(node)
{
	this.right = node.right;
	this.left = node.left;
	this.key = node.key;
	this.value = node.value;
	
	if(this.left) node.left.parent = this;
	if(this.right) node.right.parent = this;
}

/**
 * Factory method to create new subtrees. Must be implemented by subclasses
 * to provide the BST add algorithm with customized subtrees to each subclass,
 * otherwise all subtrees will be instances of BinarySearchTree
 * 
 * @method createNewChild
 * @param {Function} comparator Function used to sort the BST
 * @param {object} [config] Defines settings for the creation of the node
 * @param {Integer} [config.key] Key to the root of the new BST
 * @param {Integer} [config.value] Key to the root of the new BST
 * @param {BinarySearchTree} [config.parent] Parent tree of the new BST
 * 
 * @return {BinarySearchTree} A new tree
 */ 

BinarySearchTree.prototype.createNewChild = function(comparator, config)
{
	return new BinarySearchTree(comparator, config);
}

/**
 * Called when a subtree is about to be removed from the
 * tree. Intended to provide subclasses with a way to react
 * to removals.
 * 
 * @method willRemoveDescendant
 * @param {BinarySearchTree} subtree The subtree from which a descendant will be removed
 * @return void
 */ 
BinarySearchTree.prototype.willRemoveDescendant = function(subtree){};

/**
 * Called when a child-tree has just been added to the 
 * tree. Intended to provide subclasses with a way to react
 * to annexations.
 * 
 * @method justAddedDescendant
 * @param {BinarySearchTree} newChild The subtree being added to the tree
 * @return void
 */ 
BinarySearchTree.prototype.justAddedDescendant = function(subtree){};