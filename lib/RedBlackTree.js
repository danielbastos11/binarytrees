/** Constructor
 * 
 * @class RedBlackTree
 * @constructor
 * 
 * @param {Function} comparator Function used to sort the BST
 * @param {object} [config] Defines settings for the creation of the node
 * @param {Integer} [config.key] Key to the root of the new BST
 * @param {Integer} [config.value] Value to the root of the new BST
 * @param {Boolean} [config.black] Tells whether the new tree is black or not
 * @param {BinarySearchTree} [config.parent] Parent tree of the new BST
 */ 
RedBlackTree = function(comparator, config){
	//Make the object a BinarySearchTree
	BalancedTree.call(this, comparator, config);
	
	// New node is black if it is the root or creator tells it to be

	Object.defineProperties(this, (function(){
		// Self-invoking function intended to provide us with two 
		// private variables (_black and _red) that will in fact
		// control the color of the node.
		// 
		// By using ".call(this)" to call the self-invoking function,
		// we can rest assured that "this" can be used properly inside
		// its scope
		
		var _black = this.isRoot() || (config && config.black ) ? true : false;
		var _red = !_black;
		
		// Actual block where the getter and the setter are defined
		return {
			"black": {
				get: function(){
					return _black;
				},
				set: function(value){
					if(this.isRoot() || value){
						_black = true;
						_red = false;
					} else {
						_black = false;
						_red = true;
					}
				}
			},
			"red": {
				get: function(){
					return _red;
				},
				set: function(value){
					if(this.isRoot() || !value){
						_black = true;
						_red = false;
					} else {
						_black = false;
						_red = true;
					}
				}
			}
		}
	}).call(this)); 
}

// Inherit prototype from BalancedTree (and hence from BinarySearchTree)
RedBlackTree.prototype = Object.create(BalancedTree.prototype);
RedBlackTree.prototype.constructor = RedBlackTree;

/**
 * React to insertions in one of its descendants
 * 
 * @param {AVLTree} subtree The subtree from which a node has been removed
 */  
RedBlackTree.prototype.justAddedDescendant = function(subtree){
	if(this.red && subtree.red)
	{
		var sibling = this.getSibling();
		if(sibling && sibling.red)
		{
			this.parent.red = true;
			sibling.black = true;
			this.black = true;
			
			this.parent.justAddedDescendant(this);
		} else
		{
			var newRoot = this.parent;
			
			if(this.isLeftChild() && subtree.isLeftChild()){
				this.parent.simpleRightRotation();
			}
			else if(this.isLeftChild() && subtree.isRightChild()){
				this.parent.doubleRightRotation();
			}
			else if(this.isRightChild() && subtree.isRightChild()){
				this.parent.simpleLeftRotation();
			}
			else if(this.isRightChild() && subtree.isLeftChild()){
				this.parent.doubleLeftRotation();
			}
			
			newRoot.black = true;
			newRoot.left.red = true;
			newRoot.right.red = true;
		}
	}
	
	if(this.red)
		this.parent.justAddedDescendant(this);
}

/**
 * Prepare to deletion in one of its descendants
 * 
 * @param {AVLTree} subtree The subtree in which a node has been deleted
 */ 
RedBlackTree.prototype.willRemoveDescendant = function(subtree){
	// We only have anything to fix if the subtree is black
	if(subtree.red) return;
	
	if(subtree.isRightChild())
		this.helperMethods.adjustForRightChildSubtree(subtree);
	else
		this.helperMethods.adjustForLeftChildSubtree(subtree);
	
}

RedBlackTree.prototype.helperMethods = {
	adjustForLeftChildSubtree: function(subtree){
		var brother = subtree.getSibling();
		
		// Caso 1
		if(brother.red){
			if(brother.left) 
				brother.left.red = true;
			subtree.parent.black = true;
			subtree.red = true;
			
			subtree.parent.simpleLeftRotation();
		}

		if(brother.black){
			var brotherLeftIsBlack = !brother.left || brother.left.black ? true : false;
			var brotherRightIsBlack = !brother.right || brother.right.black ? true : false;

			// Caso 2
			if(brotherRightIsBlack && brotherLeftIsBlack){
				subtree.parent.black = true;
				brother.red = true;
			} else {
				// Passa caso 3 pro 4
				if(brotherRightIsBlack){
					brother.red = true;
					brother.left.black = true;
					brother.simpleRightRotation();
				}

				// Caso 4
				brother.black = subtree.parent.black;
				if(brother.right)
					brother.right.black = true;
				
				subtree.parent.simpleLeftRotation();
				subtree.parent.black = true;
			}
		}
	},
	
	adjustForRightChildSubtree: function(subtree){
		var brother = subtree.getSibling();

		// Caso 1
		if(brother.red){
			if(brother.right)
				brother.right.red = true;
			subtree.parent.black = true;
			subtree.red = true;
			
			subtree.parent.simpleRightRotation();
		}

		if(brother.black){
			var brotherLeftIsBlack = !brother.left || brother.left.black ? true : false;
			var brotherRightIsBlack = !brother.right || brother.right.black ? true : false;

			// Caso 2
			if(brotherRightIsBlack && brotherLeftIsBlack){
				subtree.parent.black = true;
				brother.red = true;
			} else {
				// Passa caso 3 pro 4
				if(brotherLeftIsBlack){
					brother.red = true;
					brother.right.black = true;
					brother.simpleLeftRotation();
				}

				// Caso 4
				brother.black = subtree.parent.black;
				if(brother.left)
					brother.left.black = true;
				
				subtree.parent.simpleRightRotation();
				subtree.parent.black = true;
			}
		}
	}
}

/**
 * Returns the sibling of this node. This method is meant to be a 
 * helper for the RBTree's justAddedDescendant implementation.
 * 
 * @method getSibling
 * 
 * @return RedBlackTree Node's sibling
 */ 
RedBlackTree.prototype.getSibling = function(){
	if(this.parent)
		return this.isRightChild() ? this.parent.left : this.parent.right;
}


/**
 * Factory Method created for compatibility with the BinarySearchTree class
 * 
 * @param {Function} comparator Function used to sort the AVLTree
 * @param {object} [config] Defines settings for the creation of the node
 * @param {Integer} [config.key] Key to the root of the new AVLTree
 * @param {Integer} [config.value] Value to the root of the new AVLTree
 * @param {Integer} [config.black] Tells whether the new tree is black or not
 * @param {BinarySearchTree} [config.parent] Parent tree of the new BST
 * @return {AVLTree} New RedBlack Tree
 */ 
RedBlackTree.prototype.createNewChild = function(comparator, config){
	return new RedBlackTree(comparator, config);
}

/**
 * Generate a String representation of the tree
 * 
 * @return {String} String representation of the tree
 */
RedBlackTree.prototype.toString = function(){
	var color = this.red ? 'r' : 'b';
	return this.key + '(' + color + ')';;
}