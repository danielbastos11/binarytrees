function addToTree(tree, numberList){
	numberList.forEach(function(element){
		if(element >= 0)
			tree.add(element, {});
		else
			tree.remove(element * -1);
		
		console.log(element + ": " + tree.printStructure());
	});
}

arv1 = [1, 2, 3, 4, 5, 6, 7, 0];
arv2 = [50, 40, 70, 30, 45, 20, 0];
arv3 = [20, 45, 30, 70, 40, 50, 0];
arv4 = [50, 30, 60, 55, 70, 80, 0];
arv5 = [50, 40, 70, 30, 45, 90, 35, 20, 10, 0];
arv6 = [20, 45, 30, 70, 10, 40, 50, 90, 48, 25, 49, -40, -10, -70, -90, -48, -25, -30, -20, 0];
arv7 = [20, 45, 30, 70, 10, 40, 50, 90, 48, 25, 49, -50, -70, -10, -40, -45, -48, -49, -25, -20, -30, -90, 0];
arv8 = [2, 1, 3, -2, -1, -3, 1, 2, 3, -1, -2, -3, 0];
arv9 = [15, 8, 90, 44, 65, 22, 36, 78, 84, 11, 2, 19, -65, -78, -90, -15, -84, -22, -11, -2, -8, 0];

(function(){
	var comparator = function(a, b){
		if(a < b)
			return -1;
		if(a > b)
			return 1;
		if(a == b)
			return 0;
	};
	
	var avl = new AVLTree(comparator);
	addToTree(avl, arv9);
	QUnit.test('Test Balance Factor on tree', function(assert){
		assertFB(avl, assert);
	});
	QUnit.test('Test if the tree is AVL', function(assert){
		AVLfication(avl, assert);
	});
	QUnit.test('Test Structure on tree', function(assert){
		assertStructure(avl, assert);
	});
})();

// Testes
function AVLfication(tree, assert){
	assert.ok(tree.balanceFactor < 2 && tree.balanceFactor > -2, tree.key + " is an AVL Tree");
	
	if(tree.left)
		AVLfication(tree.left, assert);
	if(tree.right)
		AVLfication(tree.right, assert);
}

function assertFB(tree, assert){
	var leftHeight = tree.left ? assertFB(tree.left, assert) : 0;
	var rightHeight = tree.right ? assertFB(tree.right, assert) : 0;
	
	var realFB = leftHeight - rightHeight;
	
	assert.equal(realFB, tree.balanceFactor,
				 "Test Balance Factor for key: " + tree.key);
	
	return Math.max(leftHeight, rightHeight) + 1;
}

function assertStructure(tree, assert){
	// The structure is correct unless tree is not a father
	// for either its right or left child (or both)
	
	var errorMsg = " Wrong children:";
	var correctness = true;
	if(tree.left && tree.left.parent != tree){
		correctness = false;
		errorMsg += " L";
	}
	if(tree.right && tree.right.parent != tree){
		correctness = false;
		errorMsg += " R";
	}
	
	var right = tree.right ? tree.right.key : '';
	var left = tree.left ? tree.left.key : '';
	assert.ok(correctness, "The paternity is true for " + tree.key + "(" + left + ", " +
			  right + "). " + errorMsg);
	
	if(tree.left)
		assertStructure(tree.left, assert);
	if(tree.right)
		assertStructure(tree.right, assert);
}