/*
 *	Test BinarySearchTree(comparator:Function, [config:Object])
 */
QUnit.test("Test constructor", function(assert){
	var comparator = function(){}; // empty comparator
	
	// Check if constructor will warn against the lack of a comparator
	assert.throws(
		function(){
			new BinarySearchTree();
		}, 
		TypeError, "BST constructor throws an TypeError if comparator is missing");

	// Enforce key-value integrity
	assert.throws(
		function(){
			new BinarySearchTree(comparator, {key: 3});
		}, "BST constructor won't let you add a key without a value");
	
	assert.throws(
		function(){
			new BinarySearchTree(comparator, {value: {}});
		}, "BST constructor won't let you add a key without a value");
	
	// Check if a node passed in config.parent will be the parent of the new node
	var parentNode = new BinarySearchTree(comparator);
	var childNode = new BinarySearchTree(comparator, {parent: parentNode});
	
	assert.equal(childNode.parent, parentNode, "config.parent is the parent of a newly created node");
	
	
});

/*
 *	Test BinarySearchTree.prototype.add(key, value)
 */

QUnit.test('Test insertion/update on node level', function(assert){
	/*
	 * Variables instantiation
	 *
	 */
	
	var comparator = function(a, b){
            if(a < b)
                return -1;
            if(a > b)
                return 1;
            if(a == b)
                return 0;
	};
	
	var parentNode = new BinarySearchTree(comparator);
	
	var mediumKey = Math.ceil(Math.random() * 101);
	var lowKey = Math.floor(Math.random() * (mediumKey - 1));
	var highKey = mediumKey + Math.floor(Math.random() * 10);
	
	/*
	 *  Tests
	 */ 
	
	// Ensure it throws an error if either key or value (or both) is missing
		// both
		assert.throws(
			function(){
				parentNode.add();
			},
			"Insertion requires key/value pair"
		);
	
		// either (key)
		assert.throws(
			function(){
				parentNode.add(5, undefined);
			},
			"Insertion won't accept a key without a value"
		);
	
		// either (value)
		assert.throws(
			function(){
				parentNode.add({});
			},
			"Insertion won't accept a value without a key"
		);
	
	// Ensure the method doesn't create a child if the BST doesn't have a key
	parentNode.add(mediumKey, {});
	assert.ok(parentNode.left == undefined && parentNode.right == undefined,
			 "Insertion won't create a child if BST doesn't have a key");

	// Ensure update
	var secondValue = {val: 2};
	parentNode.add(mediumKey, secondValue);
	assert.ok(parentNode.left == undefined && parentNode.right == undefined,
			 "Inserting with an existent key won't create a new node");

	assert.equal(parentNode.value, secondValue, "Update changes the value related to the key");
	// 
	// Ensure left-child creation as appropriated
	parentNode.add(lowKey, {});
	
	assert.notEqual(parentNode.left, undefined, "Left-child created when appropriated");
	
	assert.equal(parentNode.left.parent, parentNode, "Parent is indeed its left-child's parent");
		
	// Ensure right-child creation as appropriated
	parentNode.add(highKey, {});
	
	assert.notEqual(parentNode.right, undefined, "Right-child created when appropriated");
	
	assert.equal(parentNode.right.parent, parentNode, "Parent is indeed its right-child's parent");
	
	// Ensure the parent node share the comparator with it's children
	
	assert.equal(parentNode.comparator, parentNode.right.comparator, "Parent has the same comparator as its right-child");
	assert.equal(parentNode.comparator, parentNode.left.comparator, "Parent has the same comparator as its left-child");
});